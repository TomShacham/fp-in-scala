package com.example.scalaproject.Monads

trait Maybe[+A] {
  def map[B](f: A => B): Maybe[B] = this match {
    case Just(a) => Just(f(a))
    case _ => Nothing
  }

  def getOrElse[B >: A](alt: B) =
    this match {
      case Just(a) => a
      case _ => alt
    }

  def flatMap[B](f: A => Maybe[B]): Maybe[B] =
    map(f).getOrElse(Nothing)

  def orElse[B >: A](alt: Maybe[B]): Maybe[B] = this match {
    case Just(a) => Just(a)
    case _ => alt
  }

}

case class Just[A](get: A) extends Maybe[A]

case object Nothing extends Maybe[Nothing]
