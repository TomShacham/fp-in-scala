package com.example.scalaproject.Monads

trait Either[A, B] {
  def map(f: B => B): Either[A, B]

  def flatMap(f: B => Either[A, B]): Either[A, B]
}

case class ^|[A, B](a: A) extends Either[A, B] {
  def map(f: B => B): ^|[A, B] = ^|(a)

  def flatMap(f: B => Either[A, B]) = ^|(a)
}

case class |^[A, B](b: B) extends Either[A, B] {
  def map(f: B => B): |^[A, B] = |^(f(b))

  def flatMap(f: B => Either[A, B]) = f(b)
}


