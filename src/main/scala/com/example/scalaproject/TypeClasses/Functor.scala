package com.example.scalaproject.TypeClasses

import com.example.scalaproject.Monads.{Just, Maybe}

sealed trait Functor[F[_]] {
  def pure[A](a: A): F[A]
  def fmap[A, B](fa: F[A])(f: A => B): F[B]
}

object Functor {
  implicit def listFunctor[A] = new Functor[List] {
    def pure[A](a: A): List[A] = List(a)
    def fmap[A, B](fa: List[A])(f: A => B): List[B] =
      fa map f
  }

  implicit def maybeFunctor[A] = new Functor[Maybe] {
    def pure[A](a: A): Maybe[A] = Just(a)
    def fmap[A, B](fa: Maybe[A])(f: (A) => B): Maybe[B] = fa map f
  }

}