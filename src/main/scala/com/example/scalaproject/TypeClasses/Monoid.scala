package com.example.scalaproject.TypeClasses

import com.example.scalaproject.Monads.{Nothing, Maybe}

trait Monoid[A] {
  def op(a1: A, a2: A): A
  def zero: A
}

object Monoid {
  implicit def listMonoid[A]: Monoid[List[A]] = new Monoid[List[A]] {
    def zero: List[A] = List.empty
    def op(a1: List[A], a2: List[A]): List[A] = a1 ++ a2
  }

  implicit def maybeMonoid[A]: Monoid[Maybe[A]] = new Monoid[Maybe[A]] {
    def zero: Maybe[A] = Nothing
    def op(a1: Maybe[A], a2: Maybe[A]): Maybe[A] = a1 orElse a2
  }

  def endoMonoid[A]: Monoid[A => A] = new Monoid[A => A] {
    def op(f: A => A, g: A => A) = f compose g
    val zero = (a: A) => a
  }
}

