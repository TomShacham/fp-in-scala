package com.example.scalaproject.TypeClasses

trait Monad[M[_]] {
  def map[A,B](m: M[A])(f: A => B): M[B]
  def flatMap[A,B](m: M[A])(f: A => M[B]): M[B]
}

object Monad {
  implicit val listMonad: Monad[List] = new Monad[List]  {
    def getOrElse[A](els: List[A]): List[A] = els
    override def map[A, B](m: List[A])(f: (A) => B): List[B] = m map f
    override def flatMap[A, B](m: List[A])(f: (A) => List[B]): List[B] = m.map(f).flatten
  }
}

