package com.example.scalaproject.TypeClasses

trait Applicative[A[_]] {
  def pure[B](a: B): A[B]
  def ap[B, C](f: A[B => C])(fa: A[B]): A[C]
}

object Applicative {
  implicit val optionApplicative = new Applicative[Option] {
    override def pure[B](a: B): Option[B] = Some(a)

    override def ap[B, C](f: Option[B => C])(fa: Option[B]): Option[C] = {
      for {
        ff <- f
        a <- fa
      } yield ff(a)
    }
  }

  implicit val listApplicative = new Applicative[List] {
    override def pure[B](a: B): List[B] = List(a)

    override def ap[B, C](f: List[B => C])(fa: List[B]): List[C] = {
      for {
        ff <- f
        a <- fa
      } yield ff(a)
    }
  }
}

